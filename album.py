"""
Defines Album class responsible for:
    - Storing the Album's track list for matching against user searches
    - Storing key itentifying features of the album (Spotify ID, Name, Artist List)
    - Ranking an album's relevance to a set of user track searches
"""
import logging
from utils import normalize, logistic, token_weight, len_counts_set

class Album:

    def __init__(self, api_response):
        self.api_response = api_response
        self.id = self.get_id()
        self.name = self.get_name()
        self.track_names = self.get_track_names()
        self.artists = self.get_artist_names()
        self.popularity = self.get_popularity()
        self.computed_searches = {}
        self.latest_total_match_score = 0

    def get_track_names(self):
        if 'items' in self.api_response.get('tracks', []):
            track_names = self.api_response['tracks']['items']
            return [normalize(i['name']) for i in track_names if 'name' in i]

    def get_artist_names(self):
        artists = []
        for artist in self.api_response.get('artists', []):
            if 'name' in artist:
                # formatting name before appending to final list of artists
                artists.append(normalize(artist['name'], lower=False))
        return artists

    def get_id(self):
        return self.api_response.get('id')

    def get_name(self):
        return normalize(self.api_response.get('name', ''),lower=False)

    def get_popularity(self):
        if self.api_response.get('popularity'):
            return self.api_response['popularity']/100.0
        return 0

    def track_match_score(self, search, track_name):
        """
        CORE SCORING FUNCTION 1

        Computes the relevance of an album's track to a user's search.

        Finds the number of matching words -- or tokens -- and computes
        a match score based on:
            - The % match against the track
            - The length of the track name
            - If matching tokens are part of a small set of stop words
        """

        _, search_token_counts, search_token_set = len_counts_set(search)
        track_len, track_token_counts, track_token_set = len_counts_set(track_name)
        intersect = search_token_set.intersection(track_token_set)
        intersect_size = 0
        for token in intersect:
            intersect_size += token_weight(token) * search_token_counts[token]
        match_score = intersect_size / float(track_len) * logistic(intersect_size)
        return match_score

    def update_total_match_score(self, search_list):
        """
        CORE SCORING FUNCTION 2

        Update the album's total match score as the sum of maximal track-search scores
        found by using the following method.

        Start with a total match score of 0
        Repeat while there are tracks and queries left to consider:
            - Find track-search combination with the maximum score
            - Add the maximum track-search match score to the album's total match score
            - Remove the track and search from consideration
        Update using the total match score
        """

        self.update_computed_match_scores(search_list)
        total_match_score = 0
        excluded_tracks = []    #Exclude tracks and searches that were already found as maximal in previous iterations
        excluded_searches = []
        scores_to_check = min(len(search_list), len(self.track_names))
        for _ in range(scores_to_check):
            searches_left = list(set(search_list) - set(excluded_searches))
            tracks_left = list(set(self.track_names) - set(excluded_tracks))
            best_track = tracks_left[0]
            best_search = searches_left[0]
            best_score = self.computed_searches[best_search][best_track]
            for search in searches_left:
                for track in tracks_left:
                    if self.computed_searches[search][track] > best_score:
                        best_score = self.computed_searches[search][track]
                        best_track = track
                        best_search = search
            if best_score == 0:
                break           #break included because if we search for a maximum track-search match score and find a zero,
                                #we can terminate early knowing that the remaining (track x search) matrix is all zeroes
            else:
                total_match_score += best_score
                excluded_tracks.append(best_track)
                excluded_searches.append(best_search)
        self.latest_total_match_score = total_match_score

    def compute_match_scores(self, search):
        """
        Returns all _track_match_scores for the instance's track_names
        """
        match_scores = {}
        for track in self.track_names:
            match_scores[track] = self.track_match_score(search, track)
        return match_scores

    def update_computed_match_scores(self, search_list):
        """
        Scores all tracks in an album against items in a search list.
        Does not recompute scores if they have already been computed.
        """
        for search in search_list:
            if search not in self.computed_searches:
                self.computed_searches[search] = self.compute_match_scores(search)
