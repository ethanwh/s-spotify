"""
Defines Session object that instantiated in __main__.py and is reponsible for:
    - Persisting user queries and albums
    - Updating rankings when new queries are made
    - Optimizing use of Spotify API by only querying when album IDs appear in track search responses
"""

from utils import track_search, album_lookup
from album import Album

class Session:

    def __init__(self,albums=[], search_list=[]):
        self.albums = albums
        self.search_list = search_list

    def search_handler(self, search):
        """
        Performs an initial query against the Spotify API using the user's search.

        Performs a second query against the Spotify API for additional information on
        the albums associated with tracks in the search response that are not
        already in self.albums.

        Appends new albums to self.albums.
        """
        self.search_list.append(search)
        new_albums = track_search(search)
        current_ids = self.get_album_id_set()
        to_lookup = new_albums - current_ids.intersection(new_albums) #Get album ids we haven't looked up yet
        to_add = album_lookup(to_lookup)
        for i in to_add.get('albums', []):
            self.albums.append(Album(i)) #Persist new albums in Session.albums

    def get_album_id_set(self):
        """
        Gets all Album.id's from the Album objects in self.albums and returns them as a set.
        """
        ids = [i.id for i in self.albums]
        return set(ids)

    def update_scores(self):
        """
        Ensures that Album.latest_total_match_score is up to date for all albums in self.albums.
        """
        for album in self.albums:
            album.update_total_match_score(self.search_list)

    def sort_albums(self):
        """
        Updates and sorts self.albums by relevance as defined by Album.latest_total_match_score.

        Ties are common especially on the first search. Popularity is used as a tie-breaker to
        improve performance in these cases.
        """
        self.update_scores()
        self.albums.sort(key=lambda x: (x.latest_total_match_score,x.popularity),reverse=True)
