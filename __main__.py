"""
Main method that:
    - Instantiates a Session object
    - Prompts user for input
    - Prints relavant albums in a table
"""
import sys
import logging
from utils import trim_string
import session

if __name__ == '__main__':
    logging.basicConfig(level=logging.WARNING) #Set level=logging.INFO to see API calls made to Spotify
    session = session.Session()

    ## Welcome Message
    print '''
                    Welcome to s-spotify!

     This program will help you recall the name of an album if you only remember peices
     of track names. Keep entering what you remember of track names of different songs
     on the album and s-spotify will try to help you remember what they are from!\n'''

    while True:
        ##
        ## Begin user prompt for input
        ##
        if len(session.search_list) > 0:
            search_again = ''
            while search_again not in ['y','n']:
                search_again = raw_input('\nWould you like to search for another track? (y/n): ')
                search_again = search_again.lower()
            if search_again == 'n':
                sys.exit()
        search = raw_input('\nEnter text from a title of a song: ')
        ##
        ## End user prompt for input
        ##

        ## Handle search and update rankings
        session.search_handler(search)
        session.sort_albums()
        if session.albums == []:
            print '\nOops! No relevant albums found.'
        ##
        ## Begin table output
        ##
        else:
            print '\n{0}\nList of Relevant Albums Based On {1} Track Search(es)\n{0}\n'.format('-'*52,len(session.search_list))
            print '{0}\n{1:22} | {2:40} | {3:30} | {4}\n{0}'.format('-'*120,'Album ID','Album Name','Artists','Raw Match Score')
            #print out top results, max 10
            for i in session.albums[:min(len(session.albums),10)]:
            # Print out rows. For fields of variable length (Artists, Album Name)
            # trim the output if needed using trim_string in utils.py
                artists = trim_string(', '.join(i.artists),30)
                name = trim_string(i.name,40)
                print '{:22} | {:40} | {:30} | {:.2f}'.format(i.id,name,artists,i.latest_total_match_score)
            print '-'*120
        ##
        ## End table output
        ##
