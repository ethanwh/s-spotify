# S Spotify API
Takehome assignment

## Prompt

Write a simple app (a command line app or a locally hosted web interface is fine), to help a user find an album. The user doesn't recall the name of the album, but remembers a song title(s) - possibly with some missing or reordered words - from the album. The app should prompt the user for the name of a track and, in response, show a list of (no more than 10) relevant albums. If some albums are more likely to be what the user is searching for, those results should appear closer to the top of the list than less likely results. The user should be able to enter additional track titles. With each additional title, the album list should be refined.

Use the Spotify API to get the data for the app. Write the app in Python and include with your response any instructions necessary to run the app (Python version, dependencies, etc.).

Please submit the code for your app and short answers to the following questions:

* Describe, in a way easy understood by a non-technical person, how the list of albums displayed by your app is ordered.

* The Spotify API is free, but, pretend for a moment that you had to pay per API request. What changes might you consider making to your app to minimize cost?

## Approach

1. Take a user's input and use the Spotify API search functionality to get the top 20 tracks based on the search.
2. Store all album ids associated with those tracks.
3. Use the Spotify API to get the full track lists of those albums.
4. Apply a scoring algorithm to rank the relevance of a given album to a user's searches. This is done by looking at the text in the set of searches a user has made and the text in the track titles in an album.
5. Print out results ordered by this scoring algorithm, with popularity as a tie-breaker.
6. Repeat 1-5, adding new albums if new ones appear in the search results, and update the relevance of albums with respect the the new searches the user has made.

Notes:
- Singles are not filtered out. These may be useful results in some scenarios, and if they are not desirable results for a given user session, they will be overtaken by full length albums as results are refined by a few searches.
- Although it is implied in the prompt that the user will not input words that are not relevant to their search -- although some may be missing or out of order -- the functionality will not completely break if an irrelevant or misspelled word is entered.

## Dependencies

Python=2.7.15
requests

If you have conda installed, an suitable environment can be created by running:

`conda create -n s-spotify-env python=2.7 requests`
`source activate s-spotify-env`

## Running s-spotify

* Download s-spotify
* Ensure you are in an environment where you have python 2.7 and the requests library (`pip install requests`)
* cd into the directory where the s-spotify package was downloaded (not into s-spotify itself)
* Type `python s-spotify`
* The program should prompt you for input
* For support email ethanwhoman@gmail.com
