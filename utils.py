"""
Collection of functions that support the main functionality
provided in album.py and session.py
"""

import base64
from collections import Counter
import logging
from math import exp
import re
import requests
import sys
import urllib
import unicodedata
from config import CLIENT_ID, CLIENT_SECRET

#CONSTANTS
BASE = 'https://api.spotify.com/v1'
STOP_WORD_WEIGHT = 0.7

#FUNCTIONS
def trim_string(string,trim_len):
    """
    Trims a string to a desired length and inticates truncation by appending
    an ellipses.

    trim_string("Born To Run",10) -> "Born to..."
    """
    if len(string) <= trim_len:
        return string
    else:
        string = string[:trim_len - 4] + ' ...'
        return string

def normalize(text,lower=True):
    """
    Standardizes text for reliable matching and printing
    """
    text = text.encode('ascii','ignore')
    if lower:
        text = text.lower()
    text = re.sub(r'[^a-zA-Z0-9 ]', '', text)
    text = re.sub(r'[ ]+', ' ', text)
    return text

def logistic(x):
    """
    Used to reflect increased confidence in matches if the song
    title is longer, asymptotically approaches 1.

    If the user inputs "Born" and there is a 100% match against
    a track named "Born", that gives us less information than if a user inputs
    "Born to Run" and there is a 100% match against a track by that title.

    logistic(1) -> 0.50
    logistic(3) -> 0.88
    logistic(5) -> 0.98
    """
    denominator = 1 + exp(-(x - 1))
    return 1 / denominator

def token_weight(token):
    """
    Used to weight a few extremely common words less than more unique words.
    """
    stop_words = ['an','the','and','on','a']
    weight = 1
    if token in stop_words:
        weight = STOP_WORD_WEIGHT
    return weight

def track_search(search,type='track',market='US'):
    """
    Searches Spotify for tracks using the search text passed in. Returns all
    albums associated with the first 20 tracks returned in the search.
    """
    token = get_token()
    headers = {'Accept' : 'application/json',
               'Content-Type' : 'application/json',
               'Authorization': 'Bearer {}'.format(token)}
    search = normalize(search)
    query_string = '{}/search?q={}&type={}&market={}'.format(BASE, urllib.quote(search), type, market)
    logging.info("Searching Spotify for tracks using request {}\n".format(query_string))
    try:
        response = requests.get(query_string, headers=headers)
        return _parse_albums_from_track_query(response.json())
    except:
        logging.warn("Failed request: {}".format(query_string))

def album_lookup(album_id_list):
    """
    Performs a batch lookup to get all information available on the album ids passed in
    """
    query_string = '{}/albums?ids={}'.format(BASE, '%2C'.join(album_id_list))
    token = get_token()
    headers = {'Accept' : 'application/json',
               'Content-Type' : 'application/json',
               'Authorization': 'Bearer {}'.format(token)}
    logging.info("Getting all tracks from Spotify for {} albums using request {}\n".format(len(album_id_list),query_string))
    try:
        response = requests.get(query_string, headers=headers)
        if response.status_code == 200:
            return response.json()
        return {}
    except:
        logging.warn("Failed search using query string {}".format(query_string))

def get_token():
    """
    Gets OAUTH token needed to query Spotify API using credentials in config.py
    """
    encoded_key = base64.b64encode('{}:{}'.format(CLIENT_ID, CLIENT_SECRET))
    try:
        response = requests.post('https://accounts.spotify.com/api/token',
                                data={'grant_type':'client_credentials'},
                                headers={'Authorization': 'Basic {}'.format(encoded_key)}).json()
        token = response['access_token']
        return token
    except:
        logging.error("Failed to authenticate using get_token(). Check connection or credentials in config.py")
        sys.exit()

def _parse_albums_from_track_query(track_query_response):
    """
    Parses album ids from the tracks in a Spotify API track search response
    """
    if 'tracks' in track_query_response:
        if 'items' in track_query_response['tracks']:
            track_names = track_query_response['tracks']['items']
            return set([i['album']['id'] for i in track_names if _is_valid_track(i)])
    return set()

def _is_valid_track(track_item):
    """
    Validates that track object contains desired fields.
    """
    return 'album' in track_item and 'id' in track_item

def len_counts_set(normalized_string):
    """
    When determining the intersect between a query and a track
    it was helpful to split on whitespace and have easy access
    to the resulting array's length, and the array words passed
    into constructors for set and Counter.
    """
    normalized_string_list = normalized_string.split(' ')
    normalized_string_len = len(normalized_string_list)
    normalized_string_token_counts = Counter(normalized_string_list)
    normalized_string_token_set = set(normalized_string_list)
    return normalized_string_len, normalized_string_token_counts, normalized_string_token_set
